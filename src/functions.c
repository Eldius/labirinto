/*
 * functions.c
 * Arquivo de implementa��o
 * das fun��es
 *
 *  Created on: 19/04/2012
 *      Author: Eldius
 */

#include <stdio.h>
#include <stdlib.h>
#include "structs.h"
#include "functions.h"

/**
 * Adiciona mais um passo ao caminho
 */
void addStep(struct Tpath *path, struct Tpoint *point) {

	if(point == NULL){
		printf("addStep point is null\n");
		return;
	}

	// Cria o novo passo
	Tstep *step = malloc(sizeof(struct Tstep));
	step->point = point;
	step->next = NULL;

	Tstep *aux = NULL;

	// Adiciona o novo passo ao fim do caminho
	if (path->last != NULL) {
		aux = path->last;
		aux->next = step;
		path->last = step;
	} else {
		// Inclui o primeiro passo do caminho
		path->first = step;
		path->last = step;
	}
}

/**
 * Cria um caminho vazio
 */
Tpath* newPath() {
	struct Tpath *path = malloc(sizeof(struct Tpath));
	path->first = NULL;
	path->last = NULL;

	return path;
}

/**
 * Busca o pr�ximo ponto de deslocamento
 */
Tpoint* getNextPoint(Tpath* path, int map[MAX_ROWS][MAX_COLS]) {

	Tpoint* point = path->last->point;
//	showPoint(point);

	// Posi��o atual (x1, y1)
	int x1 = point->x;
	int y1 = point->y;

	// Posi��o anterior (x0, y0)
	int x0 = 0;
	int y0 = 0;

	Tstep* step = path->first;

	// Busca pen�ltimo passo do caminho
	// para verificar posteriormente se
	// n�o est� voltando
	while((step->next!=NULL) && (step->next!=path->last)){
		step = step->next;
	}

	// Definindo a posi��o anterior
	x0 = step->point->x;
	y0 = step->point->y;

	// �ndices para navega��o no mapa
	int j = y1;
	int i = x1;

	// Variando X
	// Verificando vizinhan�a (x1-1, y1) (x1+1, y1)
	for (i = x1 - 1; i <= x1 + 1; i++) {

		// Verifica se i e j est�o em uma posi��o v�lida
		// Como y n�o varia n�o � necess�rio verific�-lo
		if ((i > 0) && (i < MAX_ROWS)) {
//			printf("Verificando ponto (%d, %d)\n", i, j);

			// Verificando se n�o esta tentando verificar a posi��o atual
			if ((i != x1) || (j != y1)) {
				// Verificando se n�o esta tentando verificar a posi��o anterior
				if ((i != x0) || (j != y0)) {
					// Verificando se � uma posi��o poss�vel (0)
					if ((map[i][j] == 0)||(map[i][j] == EXIT_VALUE)) {
//						printf("achou\n");
						Tpoint* point = malloc(sizeof(Tpoint));

						point->x = i;
						point->y = j;

						// Retornando o pr�ximo ponto para deslocamento
						return point;
					}
				}
			}
		}
	}

	j = y1;
	i = x1;

	// Variando y
	// Verificando vizinhan�a (x1, y1-1) (x1, y1+1)
	for (j = y1 - 1; j <= y1 + 1; j++) {
//		printf("Verificando ponto (%d, %d)\n", i, j);
		if ((i != x1) || (j != y1)) {
			if ((i != x0) || (j != y0)) {
				if ((map[i][j] == 0)||(map[i][j] == EXIT_VALUE)) {
//					printf("achou!");
					Tpoint* point = malloc(sizeof(Tpoint));

					point->x = i;
					point->y = j;

					return point;
				}
			}
		}
	}

	// N�o foi encontrada um novo ponto poss�vel
	return NULL;
}

// Exibir posi��o
void showPoint(Tpoint* point){
	if(point == NULL){
		printf("point == null\n");
	}else{
		printf("(%d, %d)\n", point->x, point->y);
	}
}


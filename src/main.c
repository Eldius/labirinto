/*
 * functions.h
 *
 *  Created on: 19/04/2012
 *      Author: Eldius
 */

#include <stdio.h>
#include <stdlib.h>
#include "structs.h"
#include "functions.h"

/* Map 01 */
int map1[MAX_ROWS][MAX_COLS] = {
		{1, 0, 1, 1, 1, 1, 1, 1, 1, 1},
		{1, 0, 0, 1, 1, 1, 1, 1, 1, 1},
		{1, 1, 0, 1, 1, 1, 1, 1, 1, 1},
		{1, 1, 0, 0, 0, 0, 0, 0, 0, 3},
		{1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
		{1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
		{1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
		{1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
		{1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
		{1, 1, 1, 1, 1, 1, 1, 1, 1, 1}
	};

/* Map 02 */
int map[MAX_ROWS][MAX_COLS] = {
		{1, 0, 1, 1, 1, 1, 1, 1, 1, 1},
		{1, 0, 0, 1, 1, 1, 1, 1, 1, 1},
		{1, 1, 0, 1, 1, 1, 1, 1, 1, 1},
		{1, 1, 0, 0, 0, 0, 0, 0, 0, 1},
		{1, 1, 1, 1, 1, 1, 1, 1, 0, 1},
		{1, 1, 1, 1, 0, 0, 0, 0, 0, 1},
		{1, 1, 1, 1, 0, 1, 1, 1, 1, 1},
		{1, 1, 1, 1, 0, 1, 1, 1, 1, 1},
		{1, 0, 0, 0, 0, 1, 1, 1, 1, 1},
		{1, 3, 1, 1, 1, 1, 1, 1, 1, 1}
	};


/* Map 03 */
int map3[MAX_ROWS][MAX_COLS] = {
		 {1, 0, 1, 1, 1, 1, 1, 1, 1, 1},
		 {1, 0, 1, 1, 1, 1, 1, 0, 0, 3},
		 {1, 0, 1, 1, 1, 1, 1, 0, 1, 1},
		 {1, 0, 1, 1, 1, 1, 1, 0, 1, 1},
		 {1, 0, 1, 1, 1, 1, 1, 0, 1, 1},
		 {1, 0, 1, 0, 0, 0, 1, 0, 1, 1},
		 {1, 0, 0, 0, 1, 0, 0, 0, 1, 1},
		 {1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
		 {1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
		 {1, 3, 1, 1, 1, 1, 1, 1, 1, 1}
	};

int main(void) {
	printf("Map:\n");
	int i = 0;
	int j = 0;

	// Printar o mapa no console
	for (i = 0; i < 10; i++) {
		for (j = 0; j < 10; j++) {
			printf(" %d", map[i][j]);
		}
		printf("\n");
	}

	// Defini��o do ponto inicial
	Tpoint *point = malloc(sizeof(struct Tpoint));
	point->x = 0;
	point->y = 1;

	// Exibir ponto inicial no console
	printf("In�cio: ");
	showPoint(point);

	// Cria��o do caminho vazio
	struct Tpath *path = newPath();

	// Adicionar o primeiro passo ao caminho
	// com o ponto inicial
	addStep(path, point);

	// Contador de passos
	int count = 0;

	do{
		printf("itera��o: %d\n", count);
		point = getNextPoint(path, map);
		addStep(path, point);
		showPoint(path->last->point);
		count++;
	// Repetir enquanto n�o atingir o m�ximo de passos ou encontrar o valor de sa�da
	}while((map[path->last->point->x][path->last->point->y] != EXIT_VALUE)&&(count < MAX_STEPS));


	// Exibir resultado final
	printf("Estado final: ");
	showPoint(path->last->point);

	if(map[path->last->point->x][path->last->point->y] == EXIT_VALUE){
		printf("Achou a sa�da!");
	}else{
		printf("N�o achou a sa�da...");
	}

	return 0;
}


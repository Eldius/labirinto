/*
 * structs.h
 * Header de defini��o das estruturas
 *
 *  Created on: 19/04/2012
 *      Author: Eldius
 */

#ifndef STRUCTS_H_
#define STRUCTS_H_

/*
 * Constantes:
 * MAX_COLS m�ximo de colunas no mapa
 * MAX_ROWS m�ximo de linhas no mapa
 * EXIT_VALUE valor do ponto de sa�da no mapa
 * START_VALUE valor do ponto de entrada no mapa (ainda n�o utilizado)
 * MAX_STEPS n�mero m�ximo de passos para procurar a sa�da
 */
#define MAX_COLS 10
#define MAX_ROWS 10
#define EXIT_VALUE 3
#define START_VALUE 9
#define MAX_STEPS 50

/*
 * Estrutura para representar
 * um ponto no mapa.
 */
typedef struct Tpoint {
	int x;
	int y;
} Tpoint;

/*
 * Estrutura para representar
 * um passo, cada passo possui
 * um ponto e uma refer�ncia
 * para o pr�ximo passo.
 */
typedef struct Tstep {
	struct Tpoint *point;
	struct Tstep *next;
//	struct Tstep *previous;
} Tstep;

/*
 * Estrutura para representar
 * o caminho de sa�da, possui
 * uma refer�ncia para o
 * primeiro e o �ltimo passo
 * da solu��o.
 */
typedef struct Tpath {
	struct Tstep *first;
	struct Tstep *last;
} Tpath;


#endif /* STRUCTS_H_ */

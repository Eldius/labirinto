/*
 * functions.h
 * Header de defini��o das fun��es
 *
 *  Created on: 19/04/2012
 *      Author: Eldius
 */

#ifndef FUNCTIONS_H_
#define FUNCTIONS_H_

	void addStep(struct Tpath *path, struct Tpoint *point);
	Tpath* newPath();
	Tpoint* getNextPoint(Tpath* path, int map[MAX_ROWS][MAX_COLS]);
	void showPoint(Tpoint* point);

#endif /* FUNCTIONS_H_ */
